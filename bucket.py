#!/usr/bin/python3

import json
import socket
from unidecode import unidecode
from humanize import naturalsize, intword
import datetime as dt

BBHOST = "nmlq.fr"


def load_datas():
    with open("/tmp/bucket.json", "r") as f:
        a = f.read()
    return json.loads(a)


def xy_send(ip, msg):

    msg = unidecode(msg)
    msg += "\nGenerated at %s" % str(dt.datetime.now())
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((ip, 1984))
    except Exception:
        return False
    s.send(msg.encode())
    s.close()


datas = load_datas()

msg = "Bucket usage\n"

linecount = 0

msg += """
    <table border=1 cellpadding=5>
    <tr>
        <td>Name</td>
        <td>Owner</td>
        <td>Shards</td>
        <td>Placement</td>
        <td>Object</td>
        <td>Size</td>
        <td>Used</td>
        <td>Actual</td>
    </tr>
     """

trends = ""

for data in datas:
    bucket_name = data.get("bucket", None)
    if bucket_name:
        num_shards = data.get("num_shards", None)
        placement_rule = data.get("placement_rule", None)
        owner = data.get("owner", None)
        usage = data.get("usage", None)
        if usage:
            main = usage.get("rgw.main", None)
            if main:
                linecount += 1
                size = main.get("size", 0)
                size_actual = main.get("size_actual", 0)
                size_utilized = main.get("size_utilized", 0)
                num_objects = main.get("num_objects", 0)
                msg += """
                    <tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    </tr>
                """ % (
                    bucket_name,
                    owner,
                    num_shards,
                    placement_rule,
                    intword(num_objects),
                    naturalsize(size),
                    naturalsize(size_actual),
                    naturalsize(size_utilized),
                )
                trends += "[buckets,%s.rrd]\n" % bucket_name
                trends += "DS:size:GAUGE:600:U:U %s\n" % size
                trends += "DS:size_actual:GAUGE:600:U:U %s\n" % size_actual
                trends += "DS:size_utilized:GAUGE:600:U:U %s\n" % size_utilized
                trends += "DS:obj:GAUGE:600:U:U %s\n" % num_objects


msg += "</table>"
msg += "<!-- linecount=%s -->" % linecount

msg = "status ceph.buckets green %s" % msg
trends = "data ceph.trends %s" % trends

xy_send(BBHOST, msg)
xy_send(BBHOST, trends)
